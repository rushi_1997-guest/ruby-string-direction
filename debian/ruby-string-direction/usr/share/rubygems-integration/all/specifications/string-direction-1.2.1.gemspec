# -*- encoding: utf-8 -*-
# stub: string-direction 1.2.1 ruby lib

Gem::Specification.new do |s|
  s.name = "string-direction".freeze
  s.version = "1.2.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Marc Busqu\u00E9".freeze]
  s.date = "2018-06-18"
  s.description = "https://github.com/waiting-for-dev/string-direction/".freeze
  s.email = "marc@lamarciana.com".freeze
  s.files = [".gitignore".freeze, ".yardopts".freeze, "COPYING.txt".freeze, "Dockerfile".freeze, "Gemfile".freeze, "README.md".freeze, "Rakefile".freeze, "bin/console".freeze, "bin/setup".freeze, "debian/changelog".freeze, "debian/compat".freeze, "debian/control".freeze, "debian/copyright".freeze, "debian/ruby-string-direction.docs".freeze, "debian/ruby-tests.rake".freeze, "debian/rules".freeze, "debian/source/format".freeze, "debian/watch".freeze, "docker-compose.yml".freeze, "lib/string-direction.rb".freeze, "lib/string-direction/configuration.rb".freeze, "lib/string-direction/detector.rb".freeze, "lib/string-direction/strategies/characters_strategy.rb".freeze, "lib/string-direction/strategies/dominant_strategy.rb".freeze, "lib/string-direction/strategies/marks_strategy.rb".freeze, "lib/string-direction/strategy.rb".freeze, "lib/string-direction/string_methods.rb".freeze, "lib/string-direction/version.rb".freeze, "spec/spec_helper.rb".freeze, "spec/string-direction/configuration_spec.rb".freeze, "spec/string-direction/detector_spec.rb".freeze, "spec/string-direction/strategies/characters_strategy_spec.rb".freeze, "spec/string-direction/strategies/dominant_strategy_spec.rb".freeze, "spec/string-direction/strategies/marks_strategy_spec.rb".freeze, "spec/string-direction/strategy_spec.rb".freeze, "spec/string-direction/string_methods_spec.rb".freeze, "spec/string-direction_spec.rb".freeze, "string-direction.gemspec".freeze]
  s.homepage = "https://github.com/waiting-for-dev/string-direction/".freeze
  s.licenses = ["GPL3".freeze]
  s.rubygems_version = "2.7.6".freeze
  s.summary = "Automatic detection of text direction (ltr, rtl or bidi) for strings".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<bundler>.freeze, ["~> 1.10"])
      s.add_development_dependency(%q<rake>.freeze, ["~> 10.4"])
      s.add_development_dependency(%q<rspec>.freeze, ["~> 3.3"])
      s.add_development_dependency(%q<pry>.freeze, ["~> 0.10"])
      s.add_development_dependency(%q<pry-byebug>.freeze, ["~> 3.2"])
    else
      s.add_dependency(%q<bundler>.freeze, ["~> 1.10"])
      s.add_dependency(%q<rake>.freeze, ["~> 10.4"])
      s.add_dependency(%q<rspec>.freeze, ["~> 3.3"])
      s.add_dependency(%q<pry>.freeze, ["~> 0.10"])
      s.add_dependency(%q<pry-byebug>.freeze, ["~> 3.2"])
    end
  else
    s.add_dependency(%q<bundler>.freeze, ["~> 1.10"])
    s.add_dependency(%q<rake>.freeze, ["~> 10.4"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3.3"])
    s.add_dependency(%q<pry>.freeze, ["~> 0.10"])
    s.add_dependency(%q<pry-byebug>.freeze, ["~> 3.2"])
  end
end
